{
    'name': "Flectra_Store Integration",

    'summary': """Connect the flectra store with community environment""",
    'author': "Jamotion GmbH",
    'website': "https://jamotion.ch",
    'category': 'Specific Industry Applications',
    'version': '1.0.1.0.0',
    'depends': [
        'gitlab_integration',
    ],
    'data': [
        'views/views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [],
}