import logging
import requests
from flectra import models, fields, api, _

_logger = logging.getLogger(__name__)


class GitlabRepository(models.Model):
    # Private attributes
    _inherit = 'gitlab.repository'

    # Default methods

    # Fields declaration
    store_id = fields.Char(string="Store ID", required=False)

    store_scan_result = fields.Text(
            string="Store Scan Result",
    )

    # compute and search fields, in the same order that fields declaration

    # Constraints and onchanges

    # CRUD methods

    # Action methods

    # Business methods
    @api.model
    def _sync_repositories(self):
        result = super(GitlabRepository, self)._sync_repositories()
        token = self.env['ir.config_parameter'].sudo().get_param('store.private_token')
        if not token:
            return result
        server_url = self.env['ir.config_parameter'].sudo().get_param('store.url', 'https://store.flectrahq.com')
        existing_repos = self.search([])
        r = requests.get(server_url + "/auto_scan/" + token)
        if r.status_code == 200:
            store_repos = r.json()
            for store_repo in store_repos:
                existing = existing_repos.filtered(lambda f: f.path == store_repo['name'])
                if existing:
                    existing.store_id = store_repo['repo_id']
                    existing.store_scan_result = store_repo['message']
        return result

    @api.multi
    def scan_store(self):
        token = self.env['ir.config_parameter'].sudo().get_param('store.private_token')
        server_url = self.env['ir.config_parameter'].sudo().get_param('store.url', 'https://store.flectrahq.com')

        for record in self:
            data = {
                'repo_number': record.store_id,
                'repo_token': token
            }
            r = requests.post(server_url + "/auto_scan/repo", data=data)
            if r.status_code in [200, 401, 403, 404]:
                result = r.json()
                record.store_scan_result = result['message']
            else:
                _logger.error('Error on call to store with code: %s', r.status_code)

    @api.multi
    def update_master(self):
        self.ensure_one()
        result = super(GitlabRepository, self).update_master()
        self.scan_store()

        return result
